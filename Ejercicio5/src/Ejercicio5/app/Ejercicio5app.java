package Ejercicio5.app;

public class Ejercicio5app {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//DECLARACIÓN DE VARIABLES
		int a = 1, b = 2, c = 3, d = 4;
		
		//INSTRUCCIONES
		b = c;
		
		c = a;
		
		a = d;
		
		d = b;
		
		//PRINT
		System.out.println("VALOR DE A: " + a);
		System.out.println("VALOR DE B: " + b);
		System.out.println("VALOR DE C: " + c);
		System.out.println("VALOR DE D: " + d);
		
	}

}
